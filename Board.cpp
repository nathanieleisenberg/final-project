// when you turn this in, fix the last FUNCTION
// -> you modified it but they said not to!

#include <iostream>
#include <utility>
#include <map>
#include "Board.h"
#include "CreatePiece.h"
#include "Terminal.h"

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Board::Board(){}

Board::~Board() {
	/*
	for(char r = '8'; r >= '1'; r--) {
		for(char c = 'A'; c <= 'H'; c++) {
            delete occ[std::pair<char, char>(c, r)];
        }
    }
	*/
	for (std::map<std::pair<char, char>, Piece*>::iterator it = occ.begin() ; it != occ.end() ; it++ ) {
		delete it->second;
	}
}

Board::Board(const Board &board) {
	char value = '?';
	for(char r = '8'; r >= '1'; r--) {
		for(char c = 'A'; c <= 'H'; c++) {
			const Piece* piece = board(std::pair<char, char>(c, r));
			if (piece) {
				value = piece->to_ascii();
				add_piece(std::pair<char, char>(c, r), value);
			}
			//board.deletePiece(std::pair<char, char>(c, r));
		}
	}
}

const Piece* Board::operator()(std::pair<char, char> position) const {
	try {
		return occ.at(position);
	} catch (const std::out_of_range&) {
		return NULL;
	}
}

bool Board::add_piece(std::pair<char, char> position, char piece_designator) {
	occ[position] = create_piece(piece_designator);
	if (occ[position]) {
		return true;
	}
	return false;
}

bool Board::check_pawn(std::pair<char, char> start, std::pair<char, char> end, const Board & copy_board) const {
	const Piece * startPiece = copy_board(start);
	// cannot capture vertically
	if (copy_board(end)) {
		if (!startPiece->legal_capture_shape(start, end)) {
			return false;
		}
	}

	// cannot move diagonally if there's no piece to capture
	if (startPiece->legal_capture_shape(start, end)) {
		if (copy_board(end) == NULL) {
			return false;
		}
	}

	if ((end.second - start.second == 2)) {
		// cannot skip over a piece
		std::pair<char, char> middle (start.first, (start.second + 1) );
		if (copy_board(middle) != NULL) {
			return false;
		}
	}
	if ((end.second - start.second == -2)) {
		// cannot skip over a piece
		std::pair<char, char> middle (start.first, (start.second - 1) );
		if (copy_board(middle) != NULL) {
			return false;
		}
	}

	return true;
}

bool Board::check_path(std::pair<char, char> start, std::pair<char, char> end, const Board & copy_board) const {
	int deltaCol = end.first - start.first;
	int deltaRow = end.second - start.second;
	int ctrCol = 0, ctrRow = 0;

	// iterate through movement path to see if there are any pieces in the way
	while (ctrCol != deltaCol || ctrRow != deltaRow) {
		std::pair<char, char> blocker ( (start.first + ctrCol), (start.second + ctrRow) );
		if (copy_board(blocker) != NULL && copy_board(blocker) != copy_board(start)) {
			return false;
		}

		if (ctrCol != deltaCol) {
			if (deltaCol > 0) {
				++ctrCol;
			} else {
				--ctrCol;
			}
		}
		if (ctrRow != deltaRow) {
			if (deltaRow > 0) {
				++ctrRow;
			} else {
				--ctrRow;
			}
		}
	}

	return true;
}

bool Board::can_move(std::pair<char, char> start, std::pair<char, char> end, const Board& copy_board) const {
	// cannot move a nonexistent piece; end position must exist on board
	if (copy_board(start) == NULL) {
		return false;
	}

	// end position must exist on the board
	if ( !(end.first >= 'A' && end.first <= 'H') || !(end.second >= '1' && end.second <= '8') ) {
		return false;
	}

	// cannot not make a move to its own position
	if (start.first == end.first && start.second == end.second) {
		return false;
	}

	const Piece * startPiece = copy_board(start);

	// piece's move shape must be valid
	if (!startPiece->legal_move_shape(start, end)) {
		return false;
	}

	// check if pawn adheres to its move and capture rules
	if (startPiece->to_ascii() == 'P' || startPiece->to_ascii() == 'p') {
		if (!copy_board.check_pawn(start, end, copy_board)) {
			return false;
		}
	}

	// boolean to decide if piece is bishop, queen, or rook
	bool bqr = (startPiece->to_ascii() == 'b' || startPiece->to_ascii() == 'B'
		|| startPiece->to_ascii() == 'q' || startPiece->to_ascii() == 'Q'
		|| startPiece->to_ascii() == 'r' || startPiece->to_ascii() == 'R');

	// bishop, queen, and rook cannot skip over pieces
	if (bqr && !copy_board.check_path(start, end, copy_board)) {
		return false;
	}

	// if mystery piece moves in straight line, it cannot skip over pieces
	// otherwise, it can skip over pieces; checking of blockers NOT necessary
	if (startPiece->to_ascii() == 'M' || startPiece->to_ascii() == 'm') {
		int deltaCol = end.first - start.first;
		int deltaRow = end.second - start.second;
		bool straight_move = ( (deltaRow == 0 && deltaCol != 0)
			|| (deltaCol == 0 && deltaRow != 0) || (deltaCol == deltaRow)
			|| (deltaCol == -deltaRow) );
		if (straight_move && !copy_board.check_path(start, end, copy_board)) {
			return false;
		}
	}

	return true;
}

bool Board::can_you_move_there(std::pair<char, char> end, bool white, const Board& copy_board) const {
	//Pretend as if you are the opposite color and and try to take the king.
	//Gets switched back before the function is left.
	bool oppColor = !white;
	for(char r1 = '1'; r1 <= '8'; r1++) {
		for(char c1 = 'A'; c1 <= 'H'; c1++) {
			const Piece* piece = copy_board(std::make_pair(c1, r1));
			if (piece != nullptr && piece->is_white() == oppColor) {
				if(copy_board.can_move(std::make_pair(c1, r1), end, copy_board)) {
					return true;
				}
			}
		}
	}
	return false;
}

void Board::display() const {
	std::cout << std::endl;
	Terminal terminal = Terminal();
	for(char r = '8'; r >= '1'; r--) {
		std::cout << r << ' ';
		for(char c = 'A'; c <= 'H'; c++) {
			switch (r) {
				case '1': case '3': case '5': case '7':
					if (c == 'A' || c == 'C' || c == 'E' || c == 'G') {
						terminal.color_bg(terminal.BLACK);
					} else {
						terminal.color_bg(terminal.WHITE);
					}
					break;
				default:
					if (c == 'A' || c == 'C' || c == 'E' || c == 'G') {
						terminal.color_bg(terminal.WHITE);
					} else {
						terminal.color_bg(terminal.BLACK);
					}
					break;
			}
			const Piece* piece = nullptr;
			try {
				piece = occ.at(std::pair<char, char>(c, r));;
			} catch (const std::out_of_range&) {
			}
			if (piece) {
				terminal.color_fg(false, terminal.CYAN);
				switch (piece->to_ascii()) {
					case 'P':
						std::cout << "\u2659 ";
						break;
					case 'N':
						std::cout << "\u265E ";
						break;
					case 'B':
						std::cout << "\u265D ";
						break;
					case 'R':
						std::cout << "\u265C ";
						break;
					case 'Q':
						std::cout << "\u265B ";
						break;
					case 'K':
						std::cout << "\u265A ";
						break;
					case 'M':
						std::cout << "\u262F ";
						break;
				}
				terminal.color_fg(true, terminal.MAGENTA);
				switch (piece->to_ascii()) {
					case 'p':
						std::cout << "\u2659 ";
						break;
					case 'n':
						std::cout << "\u265E ";
						break;
					case 'b':
						std::cout << "\u265D ";
						break;
					case 'r':
						std::cout << "\u265C ";
						break;
					case 'q':
						std::cout << "\u265B ";
						break;
					case 'k':
						std::cout << "\u265A ";
						break;
					case 'm':
						std::cout << "\u262F ";
						break;
				}
			} else {
				std::cout << "  ";
			}
		}
		terminal.set_default();
		std::cout << std::endl;
	}
	std::cout << "  A B C D E F G H" << std::endl << std::endl;
}

void Board::deletePiece(std::pair<char, char> position) {
	if (occ[position]) {
		try {
			delete occ.at(position);
			//return occ.at(position);
		} catch (const std::out_of_range&) {
		}
	}
}

void Board::set_occ(std::pair<char, char> position, const Piece * newPiece) {
	/*
	const Piece* piece;
	try {
		piece = occ.at(position);
	} catch (const std::out_of_range&) {
		return;
	}
	*/
	if (newPiece) {
		occ.at(position) = create_piece(newPiece->to_ascii());
	} else {
		occ.at(position) = nullptr;
		//add_piece(position, '?');
	}
}

bool Board::has_valid_kings() const {
	int blackKingCounter = 0;
	int whiteKingCounter = 0;
	for(char r = '8'; r >= '1'; r--) {
		for(char c = 'A'; c <= 'H'; c++) {
			const Piece* piece = occ.at(std::pair<char, char>(c, r));
			if (piece) {
				if (piece->to_ascii() == 'k') {
					++blackKingCounter;
				}
				if (piece->to_ascii() == 'K') {
					++whiteKingCounter;
				}
			}
		}
	}

	if(blackKingCounter == 1 && whiteKingCounter == 1) {
		return true;
	}
	else {
		return false;
	}

}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator<<(std::ostream& os, const Board& board) {
	for(char r = '8'; r >= '1'; r--) {
		for(char c = 'A'; c <= 'H'; c++) {
			const Piece* piece = board(std::pair<char, char>(c, r));
			if (piece) {
				os << piece->to_ascii();
			} else {
				os << '-';
			}
		}
		os << std::endl;
	}
	return os;
}
