///////////////////////////////////
// IT IS OK TO MODIFY THIS FILE, //
// YOU WON'T HAND IT IN!!!!!     //
///////////////////////////////////
#ifndef MYSTERY_H
#define MYSTERY_H

#include "Piece.h"

class Mystery : public Piece {

public:
	~Mystery() {}
	bool legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
		int deltaCol = end.first - start.first;
		int deltaRow = end.second - start.second;
		if (deltaRow > 0 && deltaCol == 0) {
			return true;
		}
		return false;
	}

	char to_ascii() const {
		return is_white() ? 'M' : 'm';
	}

private:
	Mystery(bool is_white) : Piece(is_white) {}

	friend Piece* create_piece(char piece_designator);
};

#endif // MYSTERY_H
