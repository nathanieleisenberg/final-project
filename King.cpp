#include "King.h"

bool King::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
    int deltaCol = end.first - start.first;
    int deltaRow = end.second - start.second;

    if (deltaCol == 1 && deltaRow == 1) {
        return true;
    } else if (deltaCol == 1 && deltaRow == -1) {
        return true;
    } else if (deltaCol == 1 && deltaRow == 0) {
        return true;
    } else if (deltaRow == 1 && deltaCol == -1) {
        return true;
    } else if (deltaRow == 1 && deltaCol == 0) {
            return true;
    } else if (deltaRow == -1 && deltaCol == -1) { 
        return true;
    } else if (deltaRow == -1 && deltaCol == 0) {
        return true;
    } else if (deltaRow == 0 && deltaCol == -1) {
        return true;
    }

    return false;
}
