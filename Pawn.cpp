#include "Pawn.h"
#include "Board.h"

bool Pawn::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
    int deltaCol = end.first - start.first;
    int deltaRow = end.second - start.second;

    if (this->is_white() && deltaCol == 0 && deltaRow == 1) {
        return true;
    } else if (this->is_white() && deltaCol == 0 && deltaRow == 2) {
        if (start.second == '2') {
            return true;
        }
    } else if (this->is_white() && deltaCol == 1 && deltaRow == 1) {
        return true;
    } else if (this->is_white() && deltaCol == -1 && deltaRow == 1) {
        return true;
    } else if (!this->is_white() && deltaCol == 0 && deltaRow == -1) {
        return true;
    } else if (!this->is_white() && deltaCol == 0 && deltaRow == -2) {
        if (start.second == '7') {
            return true;
        }
    } else if (!this->is_white() && deltaCol == -1 && deltaRow == -1) {
        return true;
    } else if (!this->is_white() && deltaCol == 1 && deltaRow == -1) {
        return true;
    }

    return false;
}

bool Pawn::legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end) const {
    int deltaCol = end.first - start.first;
    int deltaRow = end.second - start.second;
    if (this->is_white() && deltaCol == 1 && deltaRow == 1) {
        return true;
    } else if (this->is_white() && deltaCol == -1 && deltaRow == 1) {
        return true;
    } else if (!this->is_white() && deltaCol == 1 && deltaRow == -1) {
        return true;
    } else if (!this->is_white() && deltaCol == -1 && deltaRow == -1) {
        return true;
    }

    return false;
}
