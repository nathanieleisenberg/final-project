#include "Chess.h"
#include <fstream>
#include <iostream>
#include <string>
#include <cassert>

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Chess::Chess() : is_white_turn(true) {
	// Add the pawns
	for (int i = 0; i < 8; i++) {
		board.add_piece(std::pair<char, char>('A' + i, '1' + 1), 'P');
		board.add_piece(std::pair<char, char>('A' + i, '1' + 6), 'p');
	}

	// Add the rooks
	board.add_piece(std::pair<char, char>( 'A'+0 , '1'+0 ) , 'R' );
	board.add_piece(std::pair<char, char>( 'A'+7 , '1'+0 ) , 'R' );
	board.add_piece(std::pair<char, char>( 'A'+0 , '1'+7 ) , 'r' );
	board.add_piece(std::pair<char, char>( 'A'+7 , '1'+7 ) , 'r' );

	// Add the knights
	board.add_piece(std::pair<char, char>( 'A'+1 , '1'+0 ) , 'N' );
	board.add_piece(std::pair<char, char>( 'A'+6 , '1'+0 ) , 'N' );
	board.add_piece(std::pair<char, char>( 'A'+1 , '1'+7 ) , 'n' );
	board.add_piece(std::pair<char, char>( 'A'+6 , '1'+7 ) , 'n' );

	// Add the bishops
	board.add_piece(std::pair<char, char>( 'A'+2 , '1'+0 ) , 'B' );
	board.add_piece(std::pair<char, char>( 'A'+5 , '1'+0 ) , 'B' );
	board.add_piece(std::pair<char, char>( 'A'+2 , '1'+7 ) , 'b' );
	board.add_piece(std::pair<char, char>( 'A'+5 , '1'+7 ) , 'b' );

	// Add the kings and queens
	board.add_piece(std::pair<char, char>( 'A'+3 , '1'+0 ) , 'Q' );
	board.add_piece(std::pair<char, char>( 'A'+4 , '1'+0 ) , 'K' );
	board.add_piece(std::pair<char, char>( 'A'+3 , '1'+7 ) , 'q' );
	board.add_piece(std::pair<char, char>( 'A'+4 , '1'+7 ) , 'k' );
}


bool Chess::make_move(std::pair<char, char> start, std::pair<char, char> end) {
	// lowercase input positions are invalid
	if (start.first >= 'a' && start.first <= 'h') {
		std::cerr << "You must enter positions in uppercase." << std::endl;
		return false;
	}
	if (end.first >= 'a' && end.first <= 'h') {
		std::cerr << "You must enter positions in uppercase." << std::endl;
		return false;
	}

	if (!board.can_move(start, end, board)) {
		return false;
	}

	const Piece * startPiece = board(start);
	const Piece * endPiece = board(end);

	// player can only move pieces of his/her color
	if ((!is_white_turn) && (startPiece->is_white())) {
		return false;
	} else if ((is_white_turn) && (!startPiece->is_white())) {
		return false;
	}

	// cannot capture a piece of same color
	if (endPiece) {
		if (startPiece->is_white() == endPiece->is_white()) {
			return false;
		}
	}
	char startChar = startPiece->to_ascii();
	char endChar = startPiece->to_ascii();
	// move the piece to the end position
	// promote pawn to queen if reaches other side
	if (startPiece->to_ascii() == 'P' || startPiece->to_ascii() == 'p') {
		if (startPiece->is_white() && end.second == '8') {
			board.deletePiece(end);
			board.deletePiece(start);
			//board.add_piece(start, '?');
			board.add_piece(end, 'Q');
		} else if (!startPiece->is_white() && end.first == '1') {
			board.deletePiece(end);
			board.deletePiece(start);
			//board.add_piece(start, '?');
			board.add_piece(end, 'q');
		} else {
			board.deletePiece(end);
			board.deletePiece(start);
			//board.add_piece(start, '?');
			board.add_piece(end, startChar);
		}
	} else {
		board.deletePiece(end);
		board.deletePiece(start);
		//board.add_piece(start, '?');
		board.add_piece(end, startChar);
	}

	// player can't put or keep him/herself in check
	if (in_check(is_white_turn)) {
		board.add_piece(start, startChar);
		board.add_piece(end, endChar);
		return false;
	}

	// switch the turn
	is_white_turn = !is_white_turn;

	return true;
}

bool Chess::in_check(bool white) const {
	char kingChar = 'K';
	if (!white) {
		kingChar = 'k';
	}

	// traverse the board, find the king, and use can_you_move_there FUNCTION
	// to see if any of the opposing team's pieces can capture the king
	for(char r1 = '1'; r1 <= '8'; r1++) {
		for(char c1 = 'A'; c1 <= 'H'; c1++) {
        	const Piece* isKing = board(std::make_pair(c1, r1));
			if (isKing != nullptr && isKing->to_ascii() == kingChar) {
				return board.can_you_move_there(std::make_pair(c1, r1), white, board);
        	}
    	}
    }

	return false;
}

// in_check function for copy of the board (used in assessing checkmate/stalemate)
bool Chess::in_check(bool white, Board & copy_board) const {
	char kingChar = 'K';
	if (!white) {
		kingChar = 'k';
	}

	// traverse the board, find the king, and use can_you_move_there FUNCTION
	// to see if any of the opposing team's pieces can capture the king
	for(char r1 = '1'; r1 <= '8'; r1++) {
		for(char c1 = 'A'; c1 <= 'H'; c1++) {
        	const Piece* isKing = copy_board(std::make_pair(c1, r1));
			if (isKing != nullptr && isKing->to_ascii() == kingChar) {
				return copy_board.can_you_move_there(std::make_pair(c1, r1), white, copy_board);
        	}
    	}
    }

	return false;
}

bool Chess::cause_check(std::pair<char, char> start, std::pair<char, char> end, Board &copy_board) const {
	const Piece* startPiece = copy_board(start);
	const Piece* endPiece = copy_board(end);
	char startChar = '?';
	char endChar = '?';
	bool white = startPiece->is_white();
	if (startPiece) {
		startChar = startPiece->to_ascii();
	}
	if (endPiece) {
		endChar = endPiece->to_ascii();
		copy_board.deletePiece(end);
	}
	copy_board.deletePiece(start);
	copy_board.add_piece(end, startChar);
	copy_board.add_piece(start, '?');	
	if ( !in_check(white, copy_board) ) {
		copy_board.deletePiece(start);
		copy_board.deletePiece(end);
		copy_board.add_piece(start, startChar);
		copy_board.add_piece(end, endChar);
		return false; // this move doesn't cause check
	}
	copy_board.deletePiece(start);
	copy_board.deletePiece(end);
	copy_board.add_piece(start, startChar);
	copy_board.add_piece(end, endChar);
	return true; // this move results in check
}

bool Chess::has_no_escape_plan(bool white) const {
	Board backup_board(board);
	const Piece* startPiece;
	const Piece* endPiece;
	for (char c1 = 'A'; c1 <= 'H'; c1++) {
		for (char r1 = '1'; r1 <= '8'; r1++) {
			std::pair <char,char> start(c1, r1);
			startPiece = backup_board(start);
			if (startPiece != nullptr && startPiece->is_white() == white) {
				for (char c2 = 'A'; c2 <= 'H'; c2++) {
					for (char r2 = '1'; r2 <= '8'; r2++) {
						std::pair <char, char> end(c2, r2);
						endPiece = backup_board(end);

						if (endPiece) {
							if (backup_board.can_move(start, end, backup_board)
								&& !cause_check(start, end, backup_board)
								&& startPiece->is_white() != endPiece->is_white()) {
									//backup_board
									return false; // there's a way to escape check!
							}
						} else {
							if (backup_board.can_move(start, end, backup_board) &&
								!cause_check(start, end, backup_board)) {
									return false; // there's a way to escape check!
							}
						}
					}
				}
			}

		}
	}
	return true; // there is NO way to escape check, it's checkmate :(
}

bool Chess::in_mate(bool white) const {
	if (!in_check(white) || in_stalemate(white)) {
		return false;
	}

	if (has_no_escape_plan(white)) {
		return true;
	}

	return false;
}

bool Chess::in_stalemate(bool white) const {
	if (in_check(white)) {
		return false;
	}

	if (has_no_escape_plan(white)) {
		return true;
	}

	return false;
}


/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator<< (std::ostream& os, const Chess& chess) {
	// Write the board out and then either the character 'w' or the character 'b',
	// depending on whose turn it is
	(void) chess;
	return os << chess.get_board() << (chess.turn_white() ? 'w' : 'b');
}


std::istream& operator>> (std::istream& is, Chess& chess) {
	char piece;
	for (int i = 7; i >= 0; i--) {
		for (int j = 0; j < 8; j++) {
			std::pair<char, char> loadPiece( 'A'+ j , '1'+ i);
			if(is >> piece) {
				chess.getBoard()->deletePiece(loadPiece);
				chess.getBoard()->add_piece(loadPiece, piece);
			}
		}
	}
	char turn;
	is >> turn;
	if(turn == 'w') {
		*(chess.get_turn()) = true;
	}
	else {
		*(chess.get_turn()) = false;
	}

	return is;
}
