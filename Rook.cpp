#include "Rook.h"
#include "Board.h"

bool Rook::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  int deltaCol = end.first - start.first;
  int deltaRow = end.second - start.second;

  if (deltaRow == 0 && deltaCol != 0) {
      return true;
  } else if (deltaRow != 0 && deltaCol == 0) {
      return true;
  }

  return false;

}
