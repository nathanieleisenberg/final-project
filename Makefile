CC = g++
CONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

chess: Chess.o main.o Board.o CreatePiece.o Knight.o Bishop.o Queen.o King.o Pawn.o Rook.o
	$(CC) $(CFLAGS) -o chess main.o Chess.o Board.o CreatePiece.o Knight.o Bishop.o Queen.o King.o Pawn.o Rook.o

main.o: main.cpp Chess.h
		$(CC) $(CFLAGS) -c main.cpp

Chess.o: Chess.cpp Chess.h
	$(CC) $(CFLAGS) -c Chess.cpp

Board.o: Board.cpp Board.h CreatePiece.h
	$(CC) $(CFLAGS) -c Board.cpp

CreatePiece.o: CreatePiece.cpp CreatePiece.h Mystery.h
	$(CC) $(CFLAGS) -c CreatePiece.cpp

Knight.o: Knight.cpp Knight.h
	$(CC) $(CFLAGS) -c Knight.cpp

Bishop.o: Bishop.cpp Bishop.h
		$(CC) $(CFLAGS) -c Bishop.cpp

Queen.o: Queen.cpp Queen.h
		$(CC) $(CFLAGS) -c Queen.cpp

King.o: King.cpp King.h
		$(CC) $(CFLAGS) -c King.cpp

Pawn.o: Pawn.cpp Pawn.h
		$(CC) $(CFLAGS) -c Pawn.cpp

Rook.o: Rook.cpp Rook.h
		$(CC) $(CFLAGS) -c Rook.cpp

clean:
	rm -f *.o chess
