#include "Queen.h"
#include "Board.h"

bool Queen::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
    int deltaCol = end.first - start.first;
    int deltaRow = end.second - start.second;

    if (deltaCol == 0 && deltaRow > 0) {
        return true;
    } else if (deltaCol == 0 && deltaRow < 0) {
        return true;
    } else if (deltaCol > 0 && deltaRow == 0) {
        return true;
    } else if (deltaCol < 0 && deltaRow == 0) {
        return true;
    } else if (deltaCol == deltaRow && deltaCol > 0) {
        return true;
    } else if (deltaCol == deltaRow && deltaCol < 0) {
        return true;
    } else if (-deltaCol == deltaRow && deltaCol > 0) {
        return true;
    } else if (-deltaCol == deltaRow && deltaCol < 0) { 
        return true;
    }

    return false;
}
