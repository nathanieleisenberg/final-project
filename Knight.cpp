#include "Knight.h"

bool Knight::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
    int deltaCol = end.first - start.first;
    int deltaRow = end.second - start.second;

    if (deltaRow == 1 && deltaCol == 2) {
        return true;
    } else if (deltaRow == 2 && deltaCol == 1) {
        return true;
    } else if (deltaRow == 2 && deltaCol == -1) {
        return true;
    } else if (deltaRow == 1 && deltaCol == -2) {
        return true;
    } else if (deltaRow == -1 && deltaCol == -2) {
        return true;
    } else if (deltaRow == -2 && deltaCol == -1) {
        return true;
    } else if (deltaRow == -2 && deltaCol == 1) {
        return true;
    } else if (deltaRow == -1 && deltaCol == 2) {
        return true;
    }

    return false;
}
