#include "Bishop.h"

bool Bishop::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
    int deltaCol = end.first - start.first;
    int deltaRow = end.second - start.second;

    if (deltaCol == deltaRow && (deltaCol > 0)) {
        return true;
    }
    if (deltaCol == deltaRow && (deltaCol < 0)) {
        return true;
    }
    if (-deltaCol == deltaRow && (deltaCol > 0)) {
        return true;
    }
    if (-deltaCol == deltaRow && (deltaCol < 0)) {
        return true;
    }

    return false;
}
